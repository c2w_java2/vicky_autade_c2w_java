import java.util.Scanner;
class Program8{

	public static void main(String[] args){
	
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Range1 : ");
		int number1 = sc.nextInt();
		System.out.println("Enter Range2 : ");
		int number2 = sc.nextInt();

		int sum = 0;
		for(int i=number1;i<=number2;i++){
			sum+=i;
		}
		System.out.println("Sum of range is : "+sum);
	}
}

