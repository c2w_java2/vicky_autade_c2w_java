class StaticDemo{
	
	int x = 30;
	static int y = 40;
	void fun(){
		System.out.println("in fun");
	}
	static void run(){
		System.out.println("in run");
	}
	public static void main(String[] args){
		StaticDemo obj = new StaticDemo();
		System.out.println(obj.x);		
		System.out.println(obj.y);
		obj.fun();
		obj.run();		
	}
}
