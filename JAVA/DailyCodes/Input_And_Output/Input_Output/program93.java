class StaticDemo{
	
	int x = 10;
	static int y = 20;
	public static void main(String[] args){
		StaticDemo obj = new StaticDemo();
		System.out.println("In main Method");
		System.out.println(obj.x);		
		System.out.println(obj.y);		
	}
}
