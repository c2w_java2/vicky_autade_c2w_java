class InputDemo{
	
	void fun(){
		System.out.println("in fun function");
	}
	static void run(){
		System.out.println("in run function");
	}
	public static void main(String[] args){
		System.out.println("In main Method");
		run();
		InputDemo obj = new InputDemo();
		obj.fun();		
	}
}
