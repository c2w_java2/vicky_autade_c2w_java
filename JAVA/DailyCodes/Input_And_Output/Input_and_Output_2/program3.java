import java.util.Scanner;
class ScannerDemo{

	public static void main(String[] args){
	
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter name : ");
		String name = sc.next();
		System.out.println("Enter College name :");
		String clgName = sc.next();
		System.out.println("Enter Student id :");
		int studId = sc.nextInt();
		System.out.println("Enter cgpa :");
		float marks = sc.nextFloat();

		System.out.println("Student Name "+ name);
		System.out.println("Student ClgName "+ clgName);
		System.out.println("Student Id "+ studId);
		System.out.println("Student Marks "+ marks);
	}
}
