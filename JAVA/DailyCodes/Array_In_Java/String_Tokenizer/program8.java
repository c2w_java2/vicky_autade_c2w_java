import java.io.*;

class InputDemo{

	public static void main(String[] args) throws IOException{
	
		BufferedReader br =  new BufferedReader(new InputStreamReader(System.in));
		
		System.out.print("Enter  name : ");
		String name  = br.readLine();
		System.out.print("Enter society name : ");
		String scName  = br.readLine();
		System.out.print("Enter wing id : ");
		char wing  = (char)br.read();
		System.out.print("Enter flat number : ");
		int flatNo = Integer.parseInt(br.readLine());

				
		
		
		System.out.println("Name :"+name);
		System.out.println("Society Name : "+scName);
		System.out.println("Wing  : "+wing);
		System.out.println("flat number : "+flatNo);
	}
}
