class Sum_Odd_Product{
	public static void main(String[]  args){
		int num = 2469185;
		int sum = 0;
		int t_rem =1; 
		while(num > 0){
			int rem = num % 10;
			num /= 10;
			if(rem % 2 != 0){
				t_rem = rem * rem;
				sum += t_rem;
			}
		}
		System.out.println(sum);
	}
}	
