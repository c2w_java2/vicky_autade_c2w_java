class Sum_odd_product_even{
	public static void main(String[]  args){
		int o_sum = 0;
		int e_product = 1;
		int num = 2569185;
		while(num > 0){
			int rem = num % 10;
			num /=10;
			if(rem % 2 == 0){
				e_product *= rem;
			}else{
				o_sum += rem;
			}
		}
		System.out.println("Sum of Odd : "+ o_sum);
		System.out.println("Product of even : "+ e_product);
	}
}	
