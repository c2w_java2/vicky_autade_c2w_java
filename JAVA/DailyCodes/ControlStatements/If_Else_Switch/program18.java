class SwitchDemo{
	public static void main(String[] args){
		String friends = "kanha";

		System.out.println("Before Switch");

		switch(friends){
			case "Ashish":
				System.out.println("Barleys");
				break;	
			case "kanha":
				System.out.println("BMC Software");
				break;
			case "Rahul":
				System.out.println("IBM");
				break;
			default:
				System.out.println("in default state");
		}	

		System.out.println("After Switch");
	}
}
