
 
class SwitchDemo{
	public static void main(String[] args){
		int num = 5;

		System.out.println("Before Switch");

		switch(num){
			case 1:
				System.out.println("One");
	
			case 2:
				System.out.println("Two");
		
			case 3:
				System.out.println("Three");
			default:
				System.out.println("in default state");//nothing matches then this will be output
		}	

		System.out.println("After Switch");

	}
}
