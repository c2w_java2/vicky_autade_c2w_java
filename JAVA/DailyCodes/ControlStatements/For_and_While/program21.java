class SwitchDemo{
	public static void main(String[] args){
		boolean x = true;

		System.out.println("Before Switch");

		switch(x){
			case true:
				System.out.println("true");
				break;	
			case false:
				System.out.println("false");
				break;
			default:
				System.out.println("in default state");
		}	

		System.out.println("After Switch");
	}
}
