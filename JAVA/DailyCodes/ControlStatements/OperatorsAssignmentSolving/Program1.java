class IfElseDemo{

	public static void main(String[] args){
	
		int num = 0;
		if(true){		//  0       2       3       4
			System.out.println(num++ + ++num + ++num + ++num);
		}
		System.out.println(num-10);//-6
		if(num>0){//true
			num=10;
		}
		System.out.println(num);//10
	}
}

// 	Output
// 	  9
// 	  -6
// 	  10
