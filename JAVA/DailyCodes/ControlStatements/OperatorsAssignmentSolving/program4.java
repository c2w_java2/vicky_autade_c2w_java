class IfElseDemo{

	public static void main(String[] args){
		char ch = 97;
		System.out.println(ch);// a
		if(ch == 'a'){//true
			ch+=3;//100 // d
			System.out.println(ch--);//d//ch=99
		}else{
			System.out.println(ch++);
		
		}

		System.out.println(ch+=5);//104//h
	}
}

//	output
//	   a
//	   d
//	   h
