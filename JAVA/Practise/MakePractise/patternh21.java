class patternh19{
	public static void main(String[] args){
		int rows=10;
		for(int i=1;i<=rows;i++){
			for(int j=1;j<=rows-i+1;j++){
				System.out.print(" ");
			}
			for(int j=1;j<=i;j++){
				if(j==1||i==j)
					System.out.print("* ");
				else
					System.out.print("  ");
			}
			System.out.println();
		}


		for(int i=1;i<=rows-1;i++){
			for(int j=1;j<=i+1;j++){
				System.out.print(" ");
			}
			for(int j=1;j<=rows-i;j++){
				if(j==1||j==rows-i)
					System.out.print("* ");
				else
					System.out.print("  ");
			}
			System.out.println();
		
		}
	}
}
