class pattern13{
	public static void main(String[] args){
		int rows=9;
		for(int i=1;i<=rows;i++){
			for(int j=1;j<=rows-i;j++){
				System.out.print(" ");
			}
			for(int j=1;j<=i;j++){
				if((i==j&&i%2!=0)||i==rows||(j==1&&i%2!=0))
				System.out.print("* ");
				else{
					System.out.print("  ");
				}
			}
			System.out.println();
		}
	}
}
