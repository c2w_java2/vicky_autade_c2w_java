import java.util.*;
class B{

	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter rows : ");
		int rows = sc.nextInt();
		for(int i=1;i<=rows;i++){
			for(int j=1;j<=rows-(2);j++){
				if(i==1||i==rows||i==(rows/2)+1||j==1)
					System.out.print("*");				else{
						System.out.print(" ");
						if(j==rows-2){
							System.out.print(" *");
						}
				}
			}
		
			System.out.println();
		}
	}
}
