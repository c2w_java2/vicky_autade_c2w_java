class patternh6{

	public static void main(String[] args){
		int rows=10;
		int num=1;
		for(int i=1;i<=rows;i++){
			for(int j=1;j<=i;j++){
				if(num%2 != 0){
					System.out.print("1 ");
					num++;	
				}
					
				else{
					System.out.print("0 ");
					num++;
				}
					
				if(i==j && i%2 == 0)
				     num--;
			}
			System.out.println();
		}
	}
	
}
