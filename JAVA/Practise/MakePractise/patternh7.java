class patternh7{
	public static void main(String[] args){
		int rows=5;
		int num = 0;
		for(int i=1;i<=rows;i++){
			num = i;
			for(int j=1;j<=rows-i;j++){
				System.out.print("  ");
			}
			for(int j=1;j<=2*i-1;j++){
				System.out.print(num+" ");
				if(j == (i-1))
					num = 1;
				else if(j < i-1)
					num--;
				else 
					num++;	
			}
	

			System.out.println();
		}
	}
}
