class patternh10{
	public static void main(String[] args){
		int rows=10;
		for(int i=1;i<=rows;i++){
			for(int j=1;j<=i;j++){
				System.out.print("* ");
			}
			for(int j=i+1;j<=2*rows-i-1;j++){
				System.out.print("  ");
			}
			for(int j=(2*rows-i);j<=2*rows-1;j++){
				if(i==rows && j==2*rows-i)
					continue;
				System.out.print("* ");
			}
			System.out.println();
		}

		for(int i=1;i<=rows-1;i++){
			for(int j=1;j<=rows-i;j++){
				System.out.print("* ");
			}
			for(int j=1;j<=i+i-1;j++){
				System.out.print("  ");
			}
			for(int j=1;j<=rows-i;j++){
				System.out.print("* ");
			}
			System.out.println();
		}

	}
}
