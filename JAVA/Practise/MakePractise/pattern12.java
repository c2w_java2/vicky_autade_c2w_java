class pattern12{
	public static void main(String[] args){
		int rows=20;
		for(int i=1;i<=rows;i++){
			for(int j=1;j<=(rows-i);j++){
				System.out.print(" ");
			}

			for(int j=1;j<=i;j++){
				if(i==j||i==(rows/2+1)||j==1)	
					System.out.print("* ");
				else
					System.out.print("  ");
			}
			System.out.println();
		}
	}
}
