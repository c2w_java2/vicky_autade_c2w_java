class patternh17{
	public static void main(String[] args ){
		int rows =5;
		
		for(int i=1;i<=rows;i++){
			int num=i;
			for(int j=1;j<=rows-(rows-i+1);j++){
				System.out.print(" ");
			}
			for(int j=1;j<=rows-i+1;j++){
				System.out.print(num+" ");
				num++;
			}
			System.out.println();
		}
	}
}
