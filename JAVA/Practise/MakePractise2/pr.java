class Solution {
   public static int[] twoSum(int[] nums, int target) {
    int [] arr =new int[2];
        for(int i=0;i<nums.length;i++){
            for(int j=0;j<nums.length;j++){      
                if((nums[i]+nums[j])==target){
                    arr[0]=i;
                    arr[1]=j;
                    return arr;
                }
            }

        }
        return arr;
    }

    public static void main(String[] args){
        int arr1[]={3,2,4};
        int arr2[]=twoSum(arr1,6);
        System.out.println(arr2[0]);
        System.out.println(arr2[1]);
    }
}
