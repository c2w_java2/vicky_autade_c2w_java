import java.util.*;
class PrimeDemo{

	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		
		int num = sc.nextInt();
		int i = 2;
		int sqrt =(int) Math.sqrt(num);
		//System.out.println(sqrt);
		boolean isPrime=true;
		int whileLoop=0;
		while(i<=sqrt){
			if(num%i==0){
				isPrime=false;
				break;
			}else{
				isPrime=true;
			}
			whileLoop++;
			i++;
		}
		if(isPrime){
			System.out.println("prime");
		}else
			System.out.println("not prime");

		System.out.println("while loop runs : "+ whileLoop);

	}
}
