import java.util.*;
class PrimeDemoo{

	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		
		int num = sc.nextInt();
		int i = 2;
		boolean isPrime=true;
		int whileLoop=0;
		while(i<=(num/2)){
			if(num%i==0){
				isPrime=false;
				break;
			}else{
				isPrime=true;
			}
			whileLoop++;
			i++;
		}
		if(isPrime){
			System.out.println("prime");
		}else
			System.out.println("not prime");

		System.out.println("while loop runs : "+ whileLoop);

	}
}
