class Operators{
	public static void main(String[] args){
		int  num1=5; //   00000000 00000000 00000000 00000101
		int num2=3;  //   00000000 00000000 00000000 00000011

		System.out.println(num1 & num2);//  00000000 00000000 00000000 00000001
						//  1
				
		System.out.println(num1 | num2);//  00000000 00000000 00000000 00000111
						//  7

		System.out.println(num1 ^ num2);//  00000000 00000000 00000000 00000110
						//  6
		
		System.out.println(num1 << 1);//  00000000 00000000 00000000 00001010
					      //  10

		System.out.println(num1 >> 1);//  00000000 00000000 00000000 00000010
					      //  2
	
	}
	
}
