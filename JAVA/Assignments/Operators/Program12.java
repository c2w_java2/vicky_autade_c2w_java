
class Operators{
	public static void main(String[] args){
		int Sub_1_marks=10,Sub_2_marks=3;

		//Arithmetic operations
		System.out.println(Sub_1_marks + Sub_2_marks);//13
		System.out.println(Sub_1_marks - Sub_2_marks);//7
		System.out.println(Sub_1_marks * Sub_2_marks);//30
		System.out.println(Sub_1_marks / Sub_2_marks);//3
		System.out.println(Sub_1_marks % Sub_2_marks);//1

		
		System.out.println();

			
		int x=10,y=5;
		//Unary operations
		System.out.println(++x);//11
		System.out.println(--x);//10
		System.out.println(y++);//5
		System.out.println(y--);//6
		System.out.println(+x);//10
		System.out.println(-x);//-10


		System.out.println();

		//Assignment operations
		System.out.println(x+=y);//15
		System.out.println(x-=y);//10
		System.out.println(x*=y);//50
		System.out.println(x/=y);//10
		System.out.println(x%=y);//0

		System.out.println();


		//Relational Operator
		System.out.println(x<y);//true
		System.out.println(x>y);//false
		System.out.println(x>=y);//false
		System.out.println(x<=y);//true
		System.out.println(x!=y);//true
		System.out.println(x==y);//false


		System.out.println();

		//Logical operators
		boolean a=true,b=false;
		System.out.println(a && b);//false
		System.out.println(a || b);//true
		System.out.println(!b);//true
		System.out.println(!a);//false

		
		System.out.println();

		//Bitwise Operators
 		int p=10, q=2;		
		System.out.println(p & q);//2
		System.out.println(p | q);//10
		System.out.println(p^q);//8
		System.out.println(~q);//-3
		System.out.println(p<<q);//40
		System.out.println(p>>q);//2
		System.out.println(p>>q);//2

	
	}

}
