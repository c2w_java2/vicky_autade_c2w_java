import java.util.*;
class ArrayDemo{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Size : ");
		int size = sc.nextInt();
		int arr [] =new int[size];
		int even_s=0,odd_s=0;
		for(int i=0;i<arr.length;i++){
			System.out.print("\nEnter element : ");
			arr[i]=sc.nextInt();
		}

		for(int i =0;i<arr.length;i++){
			if(arr[i]%2==0)
				even_s+=arr[i];
			else
				odd_s+=arr[i];

		}

		System.out.print("\nOdd sum : "+odd_s);
		System.out.print("\nEven sum :"+even_s);
	}
}
