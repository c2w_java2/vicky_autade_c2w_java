import java.util.*;
class ArrayDemo{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Size : ");
		int size = sc.nextInt();
		int arr [] =new int[size];
		int idx = -1;
		for(int i=0;i<arr.length;i++){
			System.out.print("\nEnter element : ");
			arr[i]=sc.nextInt();
		}

		System.out.println();
		int min = Integer.MAX_VALUE;
		for(int i =0;i<arr.length;i++){
			if(min >arr[i])
				min =arr[i];
		}
		int temp  = Integer.MAX_VALUE;

		for(int i=0;i<arr.length;i++){

			if(arr[i] < temp && arr[i] > min )
				temp=arr[i];
		
		}
		System.out.println("\nSecond min element is "+temp);

			
	}
}
