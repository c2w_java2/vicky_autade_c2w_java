import java.util.Scanner;
class ArrayDemo{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of the array  : ");
		int num = sc.nextInt();
		int arr[] =new int[num];

		for(int i = 0;i<arr.length;i++){
			System.out.print("\nEnter value : ");
			arr[i] = sc.nextInt();
		}
		int max = Integer.MIN_VALUE;
		System.out.print("Entered array of characters is : ");
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+" ");
			if(max<arr[i])
				max = arr[i];
		}

		
		System.out.println("\nMinimum number in the array is : "+max);
		}
}
