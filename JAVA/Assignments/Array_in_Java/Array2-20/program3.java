import java.util.Scanner;
class ArrayDemo{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of the array  : ");
		int num = sc.nextInt();
		char arr[] =new char[num];

		for(int i = 0;i<arr.length;i++){
			System.out.print("\nEnter value : ");
			arr[i] = sc.next().charAt(0);
		}

		System.out.print("Entered array of characters is : ");
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]);
		}
		System.out.println();
		for(int i=0;i<arr.length;i++){
			if(arr[i]=='a'||arr[i]=='e'||arr[i]=='o'||arr[i]=='u'||arr[i]=='i'||arr[i]=='A'||arr[i]=='E'||arr[i]=='I'||arr[i]=='O'||arr[i]=='U'){
				System.out.println("Vowel "+arr[i] +" found at index "+i);
			}
		}

	}
}
