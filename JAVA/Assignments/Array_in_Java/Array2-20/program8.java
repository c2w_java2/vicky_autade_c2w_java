import java.util.Scanner;
class ArrayDemo{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of the array  : ");
		int num = sc.nextInt();
		int arr[] =new int[num];

		for(int i = 0;i<arr.length;i++){
			System.out.print("\nEnter value : ");
			arr[i] = sc.nextInt();
		}

		System.out.print("Entered array of characters is : ");
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+" ");
		}

		
		System.out.print("\nElements are  :");
		for(int i=0;i<arr.length;i++){
			if(arr[i]>5 && arr[i]<9){
				System.out.println(arr[i]+" is greater than 5 but less than 9 .");
			}
		}
		
		}
}
