import java.util.*;
class ArrayDemo{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter size of array  :");
		int size = sc.nextInt();
		int arr[]=new int[size];

		for(int i=0;i<arr.length;i++){
			System.out.print("\nEnter element :");
			arr[i]=sc.nextInt();
		}
		System.out.println();
		int avr = 0;
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+" ");
			avr+=arr[i];
		}
		System.out.println("\nArray element average is : "+(avr/arr.length));

	}
}
