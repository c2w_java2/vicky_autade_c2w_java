import java.util.*;
class ArrayDemo{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter size of array  :");
		int size = sc.nextInt();
		int arr[]=new int[size];

		for(int i=0;i<arr.length;i++){
			System.out.print("\nEnter element :");
			arr[i]=sc.nextInt();
		}
		System.out.println();
		int Occured = 0;
		System.out.println("Enter number for Checking  :");
		int key =sc.nextInt();

		for(int i=0;i<arr.length;i++){
			if(arr[i]==key)
				Occured++;

		}
		
		if(Occured>2)
			System.out.println("\n"+key+" Occurs more than 2 times  in the array");
		else
			System.out.println("\n"+key+" Occurs less  than  or equal 2 times  in the array");
			

	}
}
