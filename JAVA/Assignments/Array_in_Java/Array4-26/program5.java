import java.util.*;
class ArrayDemo{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter size of array  :");
		int size = sc.nextInt();
		int arr[]=new int[size];

		for(int i=0;i<arr.length;i++){
			System.out.print("\nEnter element :");
			arr[i]=sc.nextInt();
		}
		System.out.println("Array is : ");
		for(int i=0;i<arr.length;i++)
			System.out.print(arr[i]+" ");

		System.out.println("\nReverser array is  :");
		//int key =sc.nextInt();

		for(int i=0;i<arr.length/2;i++){
			int temp=arr[i];
			arr[i]=arr[arr.length-1-i];
			arr[arr.length-1-i]=temp;

		}

		
		for(int i=0;i<arr.length;i++)
			System.out.print(arr[i]+" ");
		System.out.println();		

	}
}
