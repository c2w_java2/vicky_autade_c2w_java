import java.util.*;
class ArrayDemo{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter size of array  :");
		int size = sc.nextInt();
		char arr[]=new char[size];
		int cnt_v=0,cnt_c=0;
		for(int i=0;i<arr.length;i++){
			System.out.print("\nEnter element :");
			arr[i]=sc.next().charAt(0);
		}

		System.out.println("Array is : ");
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+" ");
			if(!(arr[i]>='a' && arr[i]<='z'))	
				arr[i]='#';
		}

		System.out.println("\n\nArray After Changes is : ");
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+ " ");
		}

		
		System.out.println();
	}
}
