import java.util.*;
class ArrayDemo{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter size of array  :");
		int size = sc.nextInt();
		int arr[]=new int[size];

		for(int i=0;i<arr.length;i++){
			System.out.print("\nEnter element :");
			arr[i]=sc.nextInt();
		}
		System.out.println();
		
		int min = Integer.MAX_VALUE;
		int max = Integer.MIN_VALUE;

		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+" ");
			if(min>arr[i])
				min=arr[i];
			if(max<arr[i])
				max=arr[i];
		}

		System.out.println("\nArray Difference betwn min and max is : "+(max-min));

	}
}
