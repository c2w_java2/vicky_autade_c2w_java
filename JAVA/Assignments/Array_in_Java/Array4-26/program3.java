import java.util.*;
class ArrayDemo{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter size of array  :");
		int size = sc.nextInt();
		int arr[]=new int[size];

		for(int i=0;i<arr.length;i++){
			System.out.print("\nEnter element :");
			arr[i]=sc.nextInt();
		}
		System.out.println();
		int max = Integer.MIN_VALUE;

		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+" ");
			if(max<arr[i])
				max=arr[i];
		}
		int temp=0;
		for(int i=0;i<arr.length;i++){
			if(arr[i]>temp && arr[i]<max)
				temp = arr[i];
		}

		System.out.println("\nSecond largest  : "+(temp));

	}
}
