import java.util.*;
class ArrayDemo{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter size of array  :");
		int size = sc.nextInt();
		char arr[]=new char[size];

		for(int i=0;i<arr.length;i++){
			System.out.print("\nEnter element :");
			arr[i]=sc.next().charAt(0);
		}
		System.out.println();

		int Occured = 0;
		System.out.println("Enter number for Checking  :");
		int key =sc.next().charAt(0);

		for(int i=0;i<arr.length;i++){
			if(arr[i]==key)
				Occured++;

		}
		
		if(Occured!=0)
			System.out.println("\n"+(char)key+" Occurs  "+Occured+" times  in the array");
		else
			System.out.println("\n"+(char)key+" Doesn't Occurs  in the array");
			

	}
}
