import java.util.Scanner;
class ArrayDemo{
	public static void main(String [] args){
		Scanner sc =  new Scanner(System.in);
		int arr[] = new int[10];
		for(int i=0;i<arr.length;i++){
			System.out.println("Enter data "+(i+1)+" :");
			arr[i]=sc.nextInt();
		}
		for(int i=0;i<arr.length;i++){
			if(i%2==1)
				System.out.println(arr[i]+" is a odd indexed element ");
		}
		
	}
}
