import java.util.Scanner;
class ArrayDemo{
	public static void main(String [] args){
		Scanner sc =  new Scanner(System.in);
		int arr[] = new int[10];
		int sum = 0;
		for(int i=0;i<arr.length;i++){
			System.out.println("Enter data "+(i+1)+" :");
			arr[i]=sc.nextInt();
		}

		for(int i=0;i<arr.length;i++){
			if(arr[i]%2==1)
				sum+=arr[i];
		}
		System.out.println("sum of  odd is : "+sum);
		
	}
}
