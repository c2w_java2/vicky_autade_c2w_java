import java.util.Scanner;
class ArrayDemo{
	public static void main(String[] args){
		Scanner sc =new Scanner(System.in);
		System.out.println("Enter the number of elements : ");
		int n = sc.nextInt();

		int arr[] = new int[n];
		for(int i=0;i<arr.length;i++){
			System.out.println("Enter element : ");
			arr[i]=sc.nextInt();
		}
		System.out.println("Array is :");
		for(int i=0;i<arr.length;i++)
			System.out.print(arr[i]+" ");

		System.out.println("\n\nArray after changes  :");
		//int key = sc.nextInt();
		//int cnt = -1;
		for(int i=0;i<arr.length;i++){
			if(arr[i]%2==0)
				arr[i]=0;
			else
				arr[i]=1;
			System.out.print(arr[i]+" ");
			
		}
			System.out.println();
		/*if(cnt==-1)
			System.out.println("number "+key+" not found in array.");
		else
			System.out.println("number "+key+" occured "+cnt+" times in an array .");

		*/


	}
}
