import java.util.Scanner;
class ArrayDemo{
	public static void main(String[] args){
		Scanner sc =new Scanner(System.in);
		System.out.println("Enter the number of elements : ");
		int n = sc.nextInt();

		int arr[] = new int[n];
		for(int i=0;i<arr.length;i++){
			System.out.println("Enter element : ");
			arr[i]=sc.nextInt();
		}
		System.out.println("Array before addition of 15 :");
		for(int i=0;i<arr.length;i++)
			System.out.print(arr[i]+" ");
		System.out.println("\n\nArray after addition of 15 :");
		for(int i=0;i<arr.length;i++)
			System.out.print((arr[i]+=15) +" ");
		


	}
}
