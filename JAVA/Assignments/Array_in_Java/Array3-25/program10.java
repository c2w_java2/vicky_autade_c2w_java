import java.util.Scanner;
class ArrayDemo{
	public static void main(String[] args){
		Scanner sc =new Scanner(System.in);
		System.out.println("Enter the number of elements : ");
		int n = sc.nextInt();

		int arr[] = new int[n];
		for(int i=0;i<arr.length;i++){
			System.out.println("Enter element : ");
			arr[i]=sc.nextInt();
		}
		System.out.println("Array is :");
		for(int i=0;i<arr.length;i++)
			System.out.print(arr[i]+" ");

		
		System.out.println("\n\nPrime numbers   :");
		int product = 1;
		for(int i=0;i<arr.length;i++)
		{
			boolean is_prime = true;
			int num =(int)Math.ceil(Math.sqrt(arr[i]));
			
			for(int j=2;j<=num;j++){
				if(arr[i]%j == 0){
					is_prime = false;				
					break;	
				}
			}

			if(is_prime && arr[i]>1)
				product*=arr[i];

			
		}
	
			System.out.println("Product is : "+product);


	}
}
