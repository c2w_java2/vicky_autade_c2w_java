//checkking number is in range of 1 to 1000.
class Inrange{

	public static void main(String[] args){
		int num = 302;

		if(num >= 1 && num <= 1000){
			System.out.println(num+" is in between 1 to 1000");
		}else if(!(num >= 1 && num <= 1000)){
			
			System.out.println(num+" is not in between 1 to 1000");
		}else{
			
			System.out.println("Invalid input");
		}
	
	}
}
