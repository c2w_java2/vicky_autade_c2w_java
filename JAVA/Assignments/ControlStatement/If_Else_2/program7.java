class Profit_Loss{
	public static void main(String[] args){
		int sellingPrice = 1200;
		int costPrice = 1000;
		if(sellingPrice > costPrice){
			System.out.println((sellingPrice-costPrice)+" Profit");
		}else if(sellingPrice < costPrice){
			System.out.println((costPrice-sellingPrice)+" Loss");
		}else{
			System.out.println("No Loss no Profit");
		}
	}
}
