
class Even_Odd_count{
	public static void main(String[] args){
		int num =  214367689;
		int e_count=0;
		int o_count=0;

		while(num>0){
			int rem=num%10;
			num=num/10;
			if(rem%2==0)
				e_count++;
			else
				o_count++;			
		}
		System.out.println("Even Count : "+e_count);
		System.out.println("Odd Count : "+o_count);
	}
}
