class IfElseDemo{

	public static void main(String[] args){

		char grade = 'O';

		if(grade=='O' || grade=='o')
			System.out.println("Outstanding");
		else if(grade=='A' || grade=='a')
			System.out.println("Very Good");
		else if(grade=='B' || grade=='b')
			System.out.println("Good");
		else if(grade=='P' || grade=='p')
			System.out.println("Pass");
		else if(grade=='F' || grade=='f')
			System.out.println("Fail");
		else
			System.out.println("Invalild grade");
	}
}
