class SwitchDemo{

	public static void main(String[] args){

		int num1 = 20;
		int num2 = 10;
		if(num1>=0 && num2>=0){
			int mul = num1 * num2;
			System.out.println("Their product is: "+mul);
			int rem = mul % 2;
			switch(rem){

				case 0:
					System.out.println("The product is even");
					break;
				case 1:
					System.out.println("The product is odd");
					break;
			}
		}
		else{
			System.out.println("Sorry negative numbers not allowed");
		}
	}
}
