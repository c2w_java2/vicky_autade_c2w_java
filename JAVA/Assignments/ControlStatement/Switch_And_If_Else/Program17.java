//TCS recruitment
//If person passed the aptitude exam, then only he goes for technical interview
//In technical interview, 3 questions are asked and their difficulty goes on increasing
//If the interviewee cannot solve any question, he is rejected
//If only 1 question is solved by the interviewee, he is allocated Ninjas
//If 2 questions are solved by the interviewee, he is allocated Digital
//If 3 questions are solved by the interviewee, he is allocated Prime
//But if person doesn't pass the aptitude, he gets rejected

class Interview{

	public static void main(String[] args){

		boolean passed = true;
		if(passed == true){
			int questionsSolved = 3;
			switch(questionsSolved){

				case 0:
					System.out.println("Sorry, try next time");
					break;
				case 1:
					System.out.println("Congrats, you are placed in Ninjas");
					break;
				case 2:
					System.out.println("Congrats, you are placed in Digital");
					break;
				case 3:
					System.out.println("Congrats, you are placed in Prime");
					break;
				default:
					System.out.println("Invalid input:");
			}
		}
		else{
			System.out.println("Sorry, try next time");
		}
	}
}
