class Results{

	public static void main(String[] args){

		int HCI = 76; 
		int DBMS = 85;
		int CNS = 68;
		int SPOS = 81;
		int TOC = 61;
		
		if(HCI>=40 && DBMS>=40 && CNS>=40 && SPOS>=40 && TOC>=40){

			int average = (HCI+DBMS+CNS+SPOS+TOC)/5;
			char grade;
			if(average>=75)
				grade = 'O';
			else if(average>=65)
				grade = 'A';
			else if(average>=55)
				grade = 'B';
			else
				grade = 'P';

			switch(grade){

				case 'O':
					System.out.println("First Class With Distinction");
					break;
				case 'A':
					System.out.println("First Class");
					break;
				case 'B':
					System.out.println("Second Class");
					break;
				case 'P':
					System.out.println("Pass");
					break;

			}
		}
		else
			System.out.println("You failed the course");	
	}
}
