class SwitchDemo{
	public static void main(String [] args){
		int data = 500;
		if(data < 500 ){
			switch(data){
				case 100: 
					System.out.println("100");
				case 200:
					System.out.println("200");
				case 300:
					System.out.println("300");
				case 400:
					System.out.println("400");
			}
		}else{
		
			if(data > 500){
				switch(data){
					case 600:
						System.out.println("600");
					case 700:
						System.out.println("700");
					case 800:
						System.out.println("800");
					case 900:
						System.out.println("900");
					case 1000:
						System.out.println("1000");
				}
			}else{
				System.out.println("Coder");
			}
		}
	}
}
