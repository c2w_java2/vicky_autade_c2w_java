class Ifelse_demo{

	public static void main(String[] args){
		int num = 13;
		if(num % 2 == 0 && num >10){
			System.out.println(num + " Number is even number and greater than 10");
		}else if(num % 2 == 0 && num <10){
			System.out.println(num + " Number is even number and smaller than 10");
		}else if(num % 2 == 0 && num == 10){
			System.out.println(num + " Number is even number and equal 10");
		}else if(num % 2 != 0 && num >10){
			System.out.println(num + " Number is odd number and greater than 10");
		}else{
			System.out.println(num + " Number is odd number and less than 10");
		}
	}
}
