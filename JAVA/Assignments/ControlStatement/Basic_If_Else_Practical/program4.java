class UpperCaseDemo{
	public static void main(String[] args){
		char ch='a';
		if(ch >= 65 && ch<=90){
			System.out.println(ch+" is a Uppercase Character");
		}else if(ch >=97 &&  ch<= 122){
			System.out.println(ch+" is a  Lowercase Character");
		}
		else{
			System.out.println("Not a character");
		}

	}
}
