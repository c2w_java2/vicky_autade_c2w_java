import java.util.*;
class Pattern7{
	public static void main(String[] args){
		Scanner sc =  new Scanner(System.in);
		System.out.println("Enter the number of rows:");
		int rows = sc.nextInt();		
		for(int i=1;i<=rows;i++){
			int num =1;
			for(int j=1;j<=rows;j++){
				if(j>=i){
					System.out.print(num+" ");
					num++;
				}
				else{
					System.out.print("  ");
				}
			}
			System.out.println();
		}

	} 
}
