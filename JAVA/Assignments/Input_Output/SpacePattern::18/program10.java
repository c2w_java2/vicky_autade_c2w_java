import java.util.*;
class Pattern8{
	public static void main(String[] args){
		Scanner sc =  new Scanner(System.in);
		System.out.println("Enter the number of rows:");
		int rows = sc.nextInt();		
		for(int i=1;i<=rows;i++){
			for(int j=1;j<=rows;j++){
				if(j>=i){
					if(rows%2==0){
						if(j%2==0)
							System.out.print((char)(j+64)+"  ");	
						else
							System.out.print(j+64+"  ");	
					}else{
						if(j%2==1)
							System.out.print((char)(j+64)+"  ");
						else
							System.out.print(j+64+"  ");
					}
				}else{
					System.out.print("    ");
				}
			}
			System.out.println();
		}

	} 
}
