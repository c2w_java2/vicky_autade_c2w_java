import java.util.*;

class NumberDemo{
	public static void main(String[] args){
		Scanner sc =new Scanner(System.in);
		System.out.println("\nEnter the number to check is it perfect or not ?");
		int num = sc.nextInt();
		int temp = num;
	
		int sum = 0;
		while(temp>0){
			int fact = 1;
			int rem  = temp%10;
			while(true){
				
				fact *= rem;
				rem -= 1;
				//System.out.println("fact "+fact);
				if(rem <= 1)
					break;
			}
			sum +=fact;
			temp /=10;
			//System.out.println(sum);
			
		}

		if(sum == num)
			System.out.println("\nIts a strong number .");
		else
			System.out.println("\nIts not a strong number");

	}
}
