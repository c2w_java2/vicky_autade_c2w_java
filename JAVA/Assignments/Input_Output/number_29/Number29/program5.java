import java.util.*;

class NumberDemo{
	public static void main(String[] args){
		Scanner sc =new Scanner(System.in);
		System.out.println("\nEnter the number : ");
		int num = sc.nextInt();
		int sqr = num*num;
		int temp1 = num;
		int temp2 = sqr;
		boolean is_auto = true;
		while(temp1>0){
			int rem1 = temp1%10;
			int rem2 = temp2%10;
			
			if(rem1 == rem2)
				is_auto = true;
			else{
				is_auto = false;
				break;
			}
		
			
			temp2/=10;
			temp1/=10;
		}

		if(is_auto)
			System.out.println("\nIts a Automorphic number .");
		else
			System.out.println("\nIts not a Automorphic  number");

	}
}
