import java.util.*;

class NumberDemo{
	public static void main(String[] args){
		Scanner sc =new Scanner(System.in);
		System.out.println("\nEnter the number : ");
		int num = sc.nextInt();
		int temp1 = num;
		//int temp3 = num;
		boolean is_happy = false;
		int sqr = 1;
		int sum = 0;
		while(true){
			while(temp1>0){
				int rem = temp1%10;
				sqr *= rem*rem;
				sum+=sqr;
				if(sum == 1){
					is_happy = true;
					break;
				}

				temp1/=10;
			}
			if(sum==1)
				break;	
		}

		if(is_happy)
			System.out.println("\nIts a Automorphic number .");
		else
			System.out.println("\nIts not a Automorphic  number");

	}
}
