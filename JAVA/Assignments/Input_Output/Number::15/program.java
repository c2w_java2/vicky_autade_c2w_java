import java.io.*;
class Pattern{

	public static void main(String[] args)throws IOException{
		BufferedReader br =  new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Number :");
		int number =  Integer.parseInt(br.readLine());
		int temp = 1;
		System.out.println("Factors are : ");
		while(temp<=number/2){
			if(number%temp==0)
				System.out.println(temp);
			temp++;
		}
		System.out.println(number);


	}
}
