import java.io.*;
class ReverseNumber{

	public static void main(String[] args)throws IOException{
		BufferedReader br =  new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Number :");
		int number =  Integer.parseInt(br.readLine());
		int rev = 0;
		while(number>0){
			int rem = number%10;
			rev = rev*10+rem;
			number/=10;
		}
	
			System.out.println("Reverse number is : "+rev);
	
	}
}
