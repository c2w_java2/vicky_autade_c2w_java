import java.io.*;
class Palindrome_Number{

	public static void main(String[] args)throws IOException{
		BufferedReader br =  new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Number :");
		int number =  Integer.parseInt(br.readLine());
		int temp=number;
		int rev = 0;
		while(temp>0){
			int rem = temp%10;
			rev = rev*10+rem;
			temp/=10;
		}
		if(number==rev)
			System.out.println("Number is Palindrome: "+ rev+"  "+number);
		else
			System.out.println("Number is not Palindrome: "+ rev+"  "+number);
	
	}
}
