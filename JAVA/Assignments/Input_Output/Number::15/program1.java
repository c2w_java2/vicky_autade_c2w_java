import java.io.*;
class Pattern1{

	public static void main(String[] args)throws IOException{
		BufferedReader br =  new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Number :");
		int number =  Integer.parseInt(br.readLine());
		int temp = 1;
		int count = 0;
		
		while(temp<=number/2){
			if(number%temp==0)
				count++;
			temp++;
		}
		if(count == 1)	
			System.out.println("Its a prime number : "+number);
		else
			System.out.println("Its not  a prime number : "+number);


	}
}
