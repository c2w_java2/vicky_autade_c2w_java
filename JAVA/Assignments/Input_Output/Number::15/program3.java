import java.io.*;
class Factorial{

	public static void main(String[] args)throws IOException{
		BufferedReader br =  new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Number :");
		int number =  Integer.parseInt(br.readLine());
		int temp = number;
		int fact = 1;

		while(temp>0){
			fact = fact*temp;
			temp--;
		}
		System.out.println("Factorial is : "+ fact);



	}
}
