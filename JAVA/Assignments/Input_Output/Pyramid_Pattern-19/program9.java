import  java.util.*;
class Pattern1{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number of rows : ");
		int rows = sc.nextInt();
		
		for(int i=1;i<=rows;i++){
			int num = 1;
			for(int j=1;j<=rows+i-1;j++){
				if(j<=rows-i){
					System.out.print("  ");
				}else{
					if(i%2==1){
						
						if(j<rows){
							System.out.print((char)(64 + num++)+ " ");
						}else{
							System.out.print((char)(num-- + 64)+" ");
						}
					}else{
						if(j<rows){
						
							System.out.print((char) (96 + num++) +" ");
						}else{
							System.out.print((char) (96 + num--)+" ");
						}


				}
			
			}

			}
			
			System.out.println();
		}
	}
}
