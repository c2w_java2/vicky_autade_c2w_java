import  java.util.*;
class Pattern1{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number of rows : ");
		int rows = sc.nextInt();
		for(int i=1;i<=rows;i++){
			int num = rows;
			for(int j=1;j<=rows+i-1;j++){
				if(j<rows-i+1)
					System.out.print("  ");
				else{
					if(i%2==0)
						System.out.print((char)(64+i) +" ");		
					else
						System.out.print(i +" ");
				}
			}
			System.out.println();
		}
	}
}
