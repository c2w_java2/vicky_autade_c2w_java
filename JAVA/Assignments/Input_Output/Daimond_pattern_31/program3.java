import java.util.*;
class Daimond_pattern{
	public static void main(String[] args){
		Scanner sc =new Scanner(System.in);
		System.out.println("Enter rows count : ");
		int rows  = sc.nextInt();
		int col=0,sp=0,num=0;
		for(int i=1;i<=2*rows-1;i++){
			num=0;
			if(i<=rows){
				sp = rows-i;
				col = 2*i-1;
				num = i;
			}else{
				sp = i-rows;
				col = col -2;
				num = rows-(i-rows);
			}
			for(int j=1;j<=sp;j++)
				System.out.print("\t");
			for(int j=1;j<=col;j++){

				System.out.print(num +"\t");
				if(j<=col/2)
					num--;
				else
					num++;
			}
			System.out.println();
		}
	}
}
