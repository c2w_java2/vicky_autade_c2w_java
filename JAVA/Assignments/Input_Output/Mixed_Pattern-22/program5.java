import java.util.*;
class MixedPattern{
	public static void main(String[] args){
	
		Scanner sc =new Scanner(System.in);
		System.out.println("Enter the number of rows : ");
		int rows = sc.nextInt();
		int num =3;
		for(int i=1;i<=rows;i++){
			for(int j=1;j<=rows;j++){

					if(i%2==0)
						System.out.print((num)+" ");
					else
						System.out.print((num*num)+" ");
					num++;
					if(num%14==0)
						num++;
			}
				
			System.out.println();
		}
	}
}
