import java.util.*;
class MixedPattern{
	public static void main(String[] args){
	
		Scanner sc =new Scanner(System.in);
		System.out.println("Enter the number of rows : ");
		int rows = sc.nextInt();
	
		for(int i=1;i<=rows;i++){
			int num = i;
			for(int j=1;j<=rows+i-1;j++){
				if(j<=rows-i){
					System.out.print("  ");
				}else{	
					if(j<rows)
						System.out.print((num--)+" ");
					else
						System.out.print((num++)+" ");
				}
				
			}
				
			System.out.println();
		}
	}
}
