import java.util.*;
class MixedPattern{
	public static void main(String[] args){
	
		Scanner sc =new Scanner(System.in);
		System.out.println("Enter the number of rows : ");
		int rows = sc.nextInt();
	
		for(int i=1;i<=rows;i++){
			int num = rows+i-1;
			for(int j=1;j<=rows+i-1;j++){
				if(j<=rows-i){
					System.out.print("  ");
				}else{	
					System.out.print((num)+" ");
					num--;
				}
				
			}
				
			System.out.println();
		}
	}
}
