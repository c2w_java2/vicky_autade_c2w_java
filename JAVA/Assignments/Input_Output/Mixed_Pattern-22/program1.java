import java.util.*;
class MixedPattern{
	public static void main(String[] args){
	
		Scanner sc =new Scanner(System.in);
		System.out.println("Enter the number of rows : ");
		int rows = sc.nextInt();
		int num = 1;
		for(int i=1;i<=rows;i++){
			for(int j=1;j<=rows-i;j++){
					System.out.print("   ");
			}

			for(int j=1;j<=rows-(rows-i);j++){
					if(i%2==0){
						if(j%2==0){
							System.out.print((num)+"  ");
							num+=2;
						}
						else{
							System.out.print(num+"  ");
							num+=i;
						}
					}else{
						System.out.print((num)+"  ");
						num+=2;
					}
					
				
			}
				
			System.out.println();
		}
	}
}
