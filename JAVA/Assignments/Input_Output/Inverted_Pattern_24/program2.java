import java.util.*;
class IntervedPattern{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter rows  :");
		int rows = sc.nextInt();
		int num =1;
		for(int i=1;i<=rows;i++){
			for(int j=1;j<=2*rows-i;j++){
				if(j<=i-1)
					System.out.print("  ");
				else
					System.out.print(num++ +" ");
			}
			System.out.println();
		}
	}
}
