import java.io.*;
class Pattern8{

	public static void main(String[] args)throws IOException{
		BufferedReader br =  new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter rows :");
		int rows =  Integer.parseInt(br.readLine());
	
		for(int i=1;i<=rows;i++){
			for(int j=1;j<=rows-i+1;j++){
				if(i%2 == 0)
					System.out.print((char)(rows+64-j+2-i)+"  ");
				else
					System.out.print(rows-j-i+2+"  ");
			
			}
			System.out.println();
		}
	}
}
