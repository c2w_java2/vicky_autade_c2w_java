import java.io.*;
class Pattern7{

	public static void main(String[] args)throws IOException{
		BufferedReader br =  new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter rows :");
		int rows =  Integer.parseInt(br.readLine());
	
		for(int i=1;i<=rows;i++){
			for(int j=1;j<=rows-i+1;j++){
				if(j%2 == 0)
					System.out.print((char)(rows+96-j+2-i)+"  ");
				else
					System.out.print(rows-j+1-i+1+"  ");
			
			}
			System.out.println();
		}
	}
}
