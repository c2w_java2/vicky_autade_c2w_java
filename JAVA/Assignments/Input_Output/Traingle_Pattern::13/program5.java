import java.io.*;
class Pattern5{

	public static void main(String[] args)throws IOException{
		BufferedReader br =  new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter rows :");
		int rows =  Integer.parseInt(br.readLine());

		for(int i=1;i<=rows;i++){
			for(int j=1;j<=rows-i+1;j++){
				if(i%2==1)
					System.out.print((char)(j+64)+"  ");
				else
					System.out.print((char)(j+96)+"  ");
			}
			System.out.println();
		}
	}
}
