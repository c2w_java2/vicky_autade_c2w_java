import java.io.*;
class Pattern6{

	public static void main(String[] args)throws IOException{
		BufferedReader br =  new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter rows :");
		int rows =  Integer.parseInt(br.readLine());
	
		for(int i=1;i<=rows;i++){
			int num = 1;
			int num2 = 1;
			for(int j=1;j<=rows-i+1;j++){
				if(j%2 == 0){
					System.out.print((char)(96+num)+"  ");
					num++;
				}
				else
					System.out.print(num2+++"  ");
			}
			System.out.println();
		}
	}
}
