import java.io.*;
class Pattern10{

	public static void main(String[] args)throws IOException{
		BufferedReader br =  new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter rows :");
		int rows =  Integer.parseInt(br.readLine());
		int num = rows;
		for(int i=1;i<=rows;i++){
			for(int j=1;j<=rows-i+1;j++){
				if(num%2 != 0 )
					System.out.print((char)(rows+96-j+2-i)+"  ");
				else
					System.out.print((char)(rows-j+1-i+1+64)+"  ");
				
			}
			num--;
			System.out.println();
		}
	}
}
