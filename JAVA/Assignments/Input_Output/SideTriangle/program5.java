import java.util.*;
class SideTriangle{	

	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number od rows : ");
		int rows  = sc.nextInt();
		for(int i=1;i<=2*rows-1;i++){
			int col = i<=rows? i :(2*rows - i );
			int num = i<=rows ? (rows-i+1) : (i-rows+1);
			for(int j=1;j<=col;j++)
				System.out.print((char)(num+64)+" ");
		
			System.out.println();
		}
	}
}
