import java.util.Scanner;
class Pattern10{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number :");
		long num = sc.nextLong();
		long rev=0;
		while(num>0){
			long rem = num%10;
			rev=10*rev+rem;
			num/=10;
		}
		while(rev>0){
			long rem = rev%10;
			if(rem%2!=0)
				System.out.println((rem*rem)+"  ");
			rev/=10;
		}
	}	
}
