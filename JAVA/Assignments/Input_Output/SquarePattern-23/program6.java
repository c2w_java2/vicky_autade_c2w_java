import java.io.*;
class SquareDemo{
	public static void main(String[] args)throws IOException{
		InputStreamReader isr =new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);

		System.out.println("Enter the number of rows : ");
		int rows = Integer.parseInt(br.readLine());
	
		for(int i=1;i<=rows;i++){
			int num = rows*rows;
			int n =1;
			for(int j=1;j<=rows;j++){
				if(i%2==1){	
					System.out.print(num-j+1+" ");
				
				}else{
					
					if(j==1)
						System.out.print(num +" ");
					else if(n++<3){
						System.out.print(num-5 +" ");
					}
					if(n==3){
						n=1;
						num-=5;
					}


				}

			}
			System.out.println();
		}
	}
}
