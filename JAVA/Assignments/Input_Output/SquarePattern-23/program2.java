import java.io.*;
class SquareDemo{
	public static void main(String[] args)throws IOException{
		InputStreamReader isr =new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);

		System.out.println("Enter the number of rows : ");
		int rows = Integer.parseInt(br.readLine());
		int num =rows;
		for(int i=1;i<=rows;i++){
			for(int j=1;j<=rows;j++){
				if(j>rows-i){
					System.out.print((char)(num +64)+" ");
				}else{
					System.out.print((char)(num +96)+" ");
				
				}
				num++;

			}
			System.out.println();
		}
	}
}
