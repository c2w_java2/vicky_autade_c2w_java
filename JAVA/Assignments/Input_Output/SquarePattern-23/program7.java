import java.io.*;
class SquareDemo{
	public static void main(String[] args)throws IOException{
		InputStreamReader isr =new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);

		System.out.println("Enter the number of rows : ");
		int rows = Integer.parseInt(br.readLine());
		int num = rows;
		
		for(int i=1;i<=rows;i++){
			for(int j=1;j<=rows;j++){
				if(i%2==0||i%2==1&&j%2==0)
					System.out.print(num*num+" ");
				else if(i%2==1&&j%2==1){

					System.out.print(num*num-1+" ");
				}
				num++;
								
			}
			System.out.println();
		}
	}
}
