import java.util.Scanner;
class Patterns9{
	public static void main(String[] args){
		Scanner sc =  new Scanner(System.in);

		System.out.println("Enter rows : ");
		int rows = sc.nextInt();
	
		int num2= 1;
		for(int i=1;i<=rows;i++){
			int num = 4;
			for(int j=1;j<=i;j++){
				if(j%2 == 0 )
				{	
					System.out.print((char)(96+num2)+"  ");
					num2++;
					
				}
				else{
					System.out.print(num +"  ");
					num+=2;
				}

			}
			System.out.println();
		}
	}
}
