import java.util.Scanner;
class Patterns6{
	public static void main(String[] args){
		Scanner sc =  new Scanner(System.in);

		System.out.println("Enter rows : ");
		int rows = sc.nextInt();
		int num = 1;
		for(int i=1;i<=rows;i++){

			for(int j=1;j<=i;j++){
				if(i%2 == 0)
					System.out.print((char)(64+num)+"  ");
				else
					System.out.print(j+"  ");
				num++;
			}
			System.out.println();
		}
	}
}
