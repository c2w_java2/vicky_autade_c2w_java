import java.util.Scanner;
class Patterns9{
	public static void main(String[] args){
		Scanner sc =  new Scanner(System.in);

		System.out.println("Enter rows : ");
		int rows = sc.nextInt();
	
		int num1= 1;
		for(int i=1;i<=rows;i++){
			
			for(int j=1;j<=i;j++){
				if(i%2 == 0 )
				{	
					System.out.print((char)(96+num1)+"  ");
					
					
				}
				else{
					System.out.print(num1 +"  ");
				
				}
				num1++;

			}
			System.out.println();
		}
	}
}
