import java.util.*;
class SquarePattern{
	public static void main(String[]args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number of Rows : ");
		int rows = sc.nextInt();
		int num = rows;
		for(int i=0;i<rows;i++){
			for(int j=0;j<rows;j++){
				if(num%2==0)
					System.out.print((num*num) + " ");
				else 
					System.out.print(num+" ");
				num++;
			}
			System.out.println();
		}
	}		
}
