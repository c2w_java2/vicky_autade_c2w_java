import java.util.*;
class SquarePattern{
	public static void main(String[]args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number of Rows : ");
		int rows = sc.nextInt();
		int num = rows;
		for(int i=0;i<rows;i++){
			for(int j=0;j<rows;j++){
				if(num%rows==0)
					System.out.print((char)(num+64) + " ");
				else 
					System.out.print(num+" ");
				num++;
			}
			System.out.println();
		}
	}		
}
