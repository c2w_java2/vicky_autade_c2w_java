import java.util.*;
class SquarePattern{
	public static void main(String[]args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number of Rows : ");
		int rows = sc.nextInt();
		int num = rows;
		for(int i=1;i<=rows;i++){
			for(int j=1;j<=rows;j++){
				if(num%2==1)
					System.out.print((char)(64+i) + " ");
				else 
					System.out.print(num+" ");
				num++;
			}
			System.out.println();
		}
	}		
}
